﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using NUnit.Framework;

namespace Korektor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static ObservableCollection<String> lista;

        public MainWindow()
        {
            InitializeComponent();

            lista = new ObservableCollection<String>();

            lista.Add("Słownik1");
            lista.Add("Słownik2");

            this.textBox1.ItemsSource = lista;
        }

        private void load(object Sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            Uri fileUri = null;
            if (openFileDialog.ShowDialog() == true)
            {

                fileUri = new Uri(openFileDialog.FileName);
                
            }
            
        }

        private void check(object Sender, RoutedEventArgs e)
        {

        }

        private void dics(object Sender, RoutedEventArgs e)
        {
            Window1 win1 = new Window1();
            win1.Show();
            //this.Close();
        }


    }
}
