﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using NUnit.Framework;

namespace Korektor
{
    /// <summary>
    /// Logika interakcji dla klasy Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        Uri fileUri = null;

        public Window3()
        {
            InitializeComponent();
        }

        private void load(object Sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            
            if (openFileDialog.ShowDialog() == true)
            {

                fileUri = new Uri(openFileDialog.FileName);

            }
        }

        private void addP(object Sender, RoutedEventArgs e)
        {

        }

        private void addD(object Sender, RoutedEventArgs e)
        {
            MainWindow.lista.Add(fileUri.ToString());
            this.Close();
        }

    }
}
