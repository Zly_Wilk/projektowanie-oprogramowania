﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Korektor
{
    /// <summary>
    /// Logika interakcji dla klasy Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            foreach (String s in MainWindow.lista)
            {
                listBox.Items.Add(s);
            }
        }

        private void modify(object Sender, RoutedEventArgs e)
        {

        }

        private void delete(object Sender, RoutedEventArgs e)
        {
            if (listBox.SelectedItem != null)
            {
                listBox.Items.RemoveAt(listBox.Items.IndexOf(listBox.SelectedItem));
                
            }
        }

        private void add(object Sender, RoutedEventArgs e)
        {
            Window3 win2 = new Window3();
            win2.Show();
            this.Close();
        }


    }
}
